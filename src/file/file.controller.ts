import { Controller, Get, Post, Put, Delete, Param, Body, UsePipes, UseInterceptors, UploadedFiles, UseGuards } from '@nestjs/common';
import { FileInterceptor, MulterModule } from '@nestjs/platform-express';
import { FileService } from './file.service';
import { FileDto } from './file.dto';
import { ValidationPipe } from '../shared/validation.pipe';
import * as multerGoogleStorage from 'multer-google-storage';
import { diskStorage } from 'multer';
import { editFileName, imageFileFilter } from './file.utils.upload';
import { AuthGuard } from '../user/auth/auth.guard';

@Controller()
export class FileController {
  constructor(private fileService: FileService) {
  }

  @UseGuards(AuthGuard)
  @Get('file')
  showAllFile(){
    return this.fileService.showAll();
  }

  @UseGuards(AuthGuard)
  @Post('file')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './daten',
        filename: editFileName,
      }),
    }),
  )
  
  async uploadedFile(@UploadedFiles() file) {
    const response = {
      originalname: file.originalname,
      filename: file.filename,
    };
    return response;
  }

  @UseGuards(AuthGuard)
  @Get('file/:id')
  readFile(@Param('id') id: string){
    return this.fileService.read(id);
  }

  @UseGuards(AuthGuard)
  @Put('file/:id')
  @UsePipes(new ValidationPipe())
  updateFile(@Param('id') id: string, @Body() data: FileDto){
    return this.fileService.update(id, data);
  }

  @UseGuards(AuthGuard)
  @Delete('file/:id')
  deleteFile(@Param('id') id: string){
    return this.fileService.delete(id);
  }
}
