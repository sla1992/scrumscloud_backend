import { IsString } from 'class-validator';

export class FileDto{
  @IsString()
  id: string;

  @IsString()
  url: string;

  @IsString()
  filename: string;


}
