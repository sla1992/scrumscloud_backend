import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { UserEntity } from '../user/user.entity';
import { OrganisationEntity } from '../organisation/organisation.entity';


@Entity('file')
export class FileEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('text')
  url: string;

  @Column('text')
  filename: string;

  @ManyToOne(type => OrganisationEntity, organisation => organisation.id, {onDelete: 'CASCADE', eager: true})
  fkorganisation: OrganisationEntity;
}
