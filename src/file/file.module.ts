import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MulterModule } from '@nestjs/platform-express';

import { FileEntity } from './file.entity';
import { FileService } from './file.service';
import { FileController } from './file.controller';
import { UserEntity } from '../user/user.entity';


@Module({
  imports: [MulterModule.register(
    {
    dest: './daten',
  }),
    TypeOrmModule.forFeature(
      [
        FileEntity, 
        UserEntity]
    )],
  providers: 
    [
    FileService
  ],
  controllers: 
    [
      FileController
    ]
})
export class FileModule {}

