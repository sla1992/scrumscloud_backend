import { Test, TestingModule } from '@nestjs/testing';
import { FileController } from './file.controller';
import { FileService } from './file.service';
import { ContextIdFactory } from '@nestjs/core';
import { TypeOrmModule, getRepositoryToken } from '@nestjs/typeorm';
import { UserEntity } from '../user/user.entity';
import { FileEntity } from './file.entity';
import { OrganisationEntity } from '../organisation/organisation.entity';
import { Repository, getConnectionManager, getConnection } from 'typeorm';
import { testOrmConfig } from '../../test_ormconfig';

describe('File Controller', () => {
  let controller: FileController;
  let service: FileService;
  let testfile: FileEntity;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [FileService],
      imports: [
        TypeOrmModule.forRoot( testOrmConfig),
        TypeOrmModule.forFeature([
          UserEntity,
          FileEntity,
          OrganisationEntity,
        ]),
      ],
    }).compile();

    service = await moduleRef.get<FileService>(FileService);
  });

  describe('GET /file', () => {
    it('Should return an array of all files', async () => {
      expect(await service.showAll()).toStrictEqual([]);
    });
  });
  
  describe('POST /file', () => {
    it('Should create a testfile', async () => {
      expect(await service.create({
        id: '3eed7364-10e1-4a6c-b03f-0a766f0317dc',
        filename: 'testfile',
        url: '//test/path',
      }).then(response => {
        testfile
      })).toBe(testfile);
    });
  });

  describe('DELETE /file', () => {
    it('Should delete a testfile', async () => {
      expect(await service.delete(
      "3eed7364-10e1-4a6c-b03f-0a766f0317dc"
      )).toEqual({"filename": "testfile", "fkorganisation": null, "id": "3eed7364-10e1-4a6c-b03f-0a766f0317dc", "url": "//test/path"});
    });
  });

  afterEach(async () =>{
    testOrmConfig.keepConnectionAlive = true;
  });
});

