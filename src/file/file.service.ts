import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { FileEntity } from './file.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { FileDto } from './file.dto';


@Injectable()
export class FileService {
  constructor(
    @InjectRepository(FileEntity)
    private fileRepository: Repository<FileEntity>,
  ) {}

  async showAll() {
    return await this.fileRepository.find();
  }

  async create(data: Partial<FileDto>) {
    // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
    // @ts-ignore
    const file = await this.fileRepository.create(data);
    await this.fileRepository.save(file);
    return file;
  }

  async read(id: string) {
    const file = await this.fileRepository.findOne({ where: { id } });
    if(!file){
      throw new HttpException('File not Found', HttpStatus.NOT_FOUND);
    }
    return file;
  }

  async update(id: string, data: Partial<FileDto>) {
    const file = await this.fileRepository.findOne({ where: { id } });
    if(!file){
      throw new HttpException('File not found', HttpStatus.NOT_FOUND);
    }
    // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
    // @ts-ignore
    await this.fileRepository.update({ id }, data);
    return file;
  }

  async delete(id: string) {
    const file = await this.fileRepository.findOne({ where: { id } });
    if(!file){
      throw new HttpException('File not found', HttpStatus.NOT_FOUND);
    }
    await this.fileRepository.delete({ id });
    return file;
  }
}