import { Controller, Get, Post, Put, Delete, Param, Body, UsePipes, UseGuards, Req, Logger } from '@nestjs/common';
import { Request } from 'express';
import { OrganisationService } from './organisation.service';
import { OrganisationDto } from './organisation.dto';
import { ValidationPipe } from '../shared/validation.pipe';
import { AuthGuard } from '../user/auth/auth.guard';
import { UserService } from 'src/user/user.service';
import { UserEntity } from 'src/user/user.entity';


@Controller('org')
export class OrganisationController {
  constructor(private organisationService: OrganisationService) {
  }

  @UseGuards(AuthGuard)
  @Get()
  showAllOrganisation(){
    return this.organisationService.showAll();
  }

  @UseGuards(AuthGuard)
  @Post()
  @UsePipes(new ValidationPipe())
  createOrganisation(@Req() request: Request, @Body() data: OrganisationDto){
    const user: any = request;
    return this.organisationService.create(user.id, data);
  }

  @UseGuards(AuthGuard)
  @Get(':id')
  readOrganisation(@Param('id') userId: string, @Body() data: OrganisationDto){
    return this.organisationService.read(userId);
  }

  @UseGuards(AuthGuard)
  @Put(':id')
  @UsePipes(new ValidationPipe())
  updateOrganisation(@Param('id') id: string, @Body() data: OrganisationDto){
    return this.organisationService.update(id, data);
  }

  @UseGuards(AuthGuard)
  @Delete(':id')
  deleteOrganisation(@Param('id') id: string){
    return this.organisationService.delete(id);
  }
}
