import { IsString } from 'class-validator';

export class OrganisationDto{
 
 @IsString()
  id: string;
 
 @IsString()
  title: string;
}
