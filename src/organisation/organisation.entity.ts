import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne, ManyToMany } from 'typeorm';
import { UserEntity } from '../user/user.entity';
import { FileEntity } from 'src/file/file.entity';


@Entity('org')
export class OrganisationEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('text')
  title: string;

  @OneToMany(type => OrganisationEntity, organisation => organisation.id)
  fkorganisation: OrganisationEntity[];

  @OneToMany(type => OrganisationEntity, organisation => organisation.id, {onDelete: 'CASCADE'} )
  fkorganisatione: FileEntity[];

}