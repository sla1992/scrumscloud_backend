import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { OrganisationController } from './organisation.controller';
import { OrganisationService } from './organisation.service';
import { OrganisationEntity } from './organisation.entity';
import { UserEntity } from '../user/user.entity';


@Module({
  imports: [TypeOrmModule.forFeature([OrganisationEntity, UserEntity])],
  controllers: [OrganisationController],
  providers: [OrganisationService],
})

export class OrganisationModule {}
