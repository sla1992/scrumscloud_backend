import { Test, TestingModule } from '@nestjs/testing';
import { OrganisationController } from './organisation.controller';
import { OrganisationService } from './organisation.service';
import { ContextIdFactory } from '@nestjs/core';
import { TypeOrmModule, getRepositoryToken } from '@nestjs/typeorm';
import { UserEntity } from '../user/user.entity';
import { FileEntity } from '../file/file.entity';
import { OrganisationEntity } from './organisation.entity';
import { Repository } from 'typeorm';
import { testOrmConfig } from '../../test_ormconfig';

describe('Organisation Controller', () => {
  let controller: OrganisationController;
  let orgService: OrganisationService;
  let testOrganisation: OrganisationEntity;
  let testUser: UserEntity;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [OrganisationService],
      imports: [
        TypeOrmModule.forRoot(testOrmConfig),
        TypeOrmModule.forFeature([
          UserEntity,
          FileEntity,
          OrganisationEntity
        ]),
      ],
    }).compile();
    
    orgService = await moduleRef.get<OrganisationService>(OrganisationService);
  });

  describe('Get /org', () => {
    it('Should return an array of all organisation', async () => {
      expect(await orgService.showAll()).toStrictEqual([]);
    });
  });
  
  afterEach(async () =>{
    testOrmConfig.keepConnectionAlive = true;
  });
});
