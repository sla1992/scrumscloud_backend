import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { OrganisationEntity } from './organisation.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { OrganisationDto } from './organisation.dto';
import { UserEntity } from '../user/user.entity';

@Injectable()
export class OrganisationService {
  constructor(
    @InjectRepository(OrganisationEntity)
    private organisationRepository: Repository<OrganisationEntity>,
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>
  ) {}

  async showAll() {
    return await this.organisationRepository.find();
  }

  async create(userId: string, data: Partial<OrganisationDto>) {
    const organisation = await this.organisationRepository.create(data);
    const saved = await this.organisationRepository.save(organisation);
    const user = await this.userRepository.findOne({where: { id: userId}});
    user.fkorganisation = saved;
    console.log('this is the user return ', user);
    await this.userRepository.save(user);
    return organisation;
  }

  async read(userId: string) {
    const user = await  this.userRepository.findOne({where: {id: userId}})
    const organisation = user.fkorganisation;
    console.log('this should store the organisation for current user: ',organisation);
    if(!organisation){
      throw new HttpException('Organisation not Found', HttpStatus.NOT_FOUND);
    }
    return organisation;
  }

  async update(id: string, data: Partial<OrganisationDto>) {
    const organisation = await this.organisationRepository.findOne({ where: { id } });
    if(!organisation){
      throw new HttpException('Organisation not found', HttpStatus.NOT_FOUND);
    }
    await this.organisationRepository.update({ id }, data);
    return organisation;
  }

  async delete(id: string) {
    const organisation = await this.organisationRepository.findOne({ where: { id } });
    if(!organisation){
      throw new HttpException('Organisation not found', HttpStatus.NOT_FOUND);
    }
    await this.organisationRepository.delete({ id });
    return organisation;
  }
}
