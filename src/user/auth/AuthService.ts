import { Injectable } from '@nestjs/common';
import { UserService } from '../user.service';
import { Repository } from 'typeorm';
import { UserEntity } from '../user.entity';
import { UserLoginDto } from '../user.login.dto';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
 
  constructor(private usersService: UserService,
              private jwtService: JwtService) {}

  async validateUser(data: UserLoginDto): Promise<any> {
    const user = await this.usersService.login(data);
    if (user && user.password === data.password) {
      const { password, ...result } = user;
      return result;
    }
    return null;
  }

  async login(user: UserEntity) {
    const payload = { username: user.username, email: user.email };
    return {
      access_token: this.jwtService.sign(payload),
      currentUser: { email: user.email, username: user.username },
    };
  }
}
