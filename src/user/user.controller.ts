import { Controller, Get, Post, Put, Delete, Param, Body, UsePipes, UseGuards, Req } from '@nestjs/common';
import { Request } from 'express';
import { UserService } from './user.service';
import { UserLoginDto, UserLoginRO } from './user.login.dto';
import { UserRegisterDto, UserRegisterRO } from './user.register.dto';
import { ValidationPipe } from '../shared/validation.pipe';
import { User } from './user.decorator';
import { AuthGuard } from '../user/auth/auth.guard';


@Controller()
export class UserController {
  constructor(private userService: UserService) {
  }
  
  @UseGuards(AuthGuard)
  @Get('users')
  showAllUsers() {
    return this.userService.showAll();
  }

  @Post('users/login')
  @UsePipes(new ValidationPipe())
  async login(@Body() body) {
    return await this.userService.login(body);
  }

  @Post('users/register')
  @UsePipes(new ValidationPipe())
  register(@Body() data: UserRegisterDto){
    return this.userService.register(data);
  }

  @UseGuards(AuthGuard)
  @Get('users/:id')
  readUser(@Param('id') id: string){
    return this.userService.read(id);
  }

  @UseGuards(AuthGuard)
  @Put('users/')
  @UsePipes(new ValidationPipe())
  updateUser(@Req() request: Request, @Body() data: UserRegisterDto){
    const user: any = request;
    return this.userService.update(user.id, data);
  }

  @UseGuards(AuthGuard)
  @Delete('users/:id')
  deleteUser(@Param('id') id: string){
    return this.userService.delete(id);
  }
}
