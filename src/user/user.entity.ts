import { Entity, PrimaryGeneratedColumn, Column, BeforeInsert, ManyToOne, OneToMany, ManyToMany, Unique, Index, BeforeUpdate, JoinColumn, JoinTable } from 'typeorm';
import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';
import { UserRegisterRO } from './user.register.dto';
import { OrganisationEntity } from '../organisation/organisation.entity';
import { FileEntity } from '../file/file.entity';


@Entity('user')
@Unique('UQ_NAMES', ['username', 'email'])
export class UserEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  
  @Index({unique: true})
  @Column('text',{name: 'usernameS'})
  username: string;

  @Index({unique: true})
  @Column('text', {name: 'emailS'})
  email: string;
 
  @Column('text', {name: 'password'})
  password: string;

  @ManyToOne(type => OrganisationEntity, organisation => organisation.id, {eager:true})
  @JoinColumn({name: "fkorganisation", referencedColumnName:"id"})
  fkorganisation: OrganisationEntity;

  @BeforeInsert()
  async hashPassword(){
    this.password = await bcrypt.hash(this.password, 10);
  }

  
  toResponseObject(showToken: boolean = true): UserRegisterRO {
    const { id, username, email, password } = this;
    const responseObject: any = {id, username, email, password}
    if(showToken){
      responseObject.token = this.getToken();
    }
    return responseObject;
  }

  async comparePassword(attempt: string){
    return await bcrypt.compare(attempt, this.password);
  }

  private getToken(){
    const {id, username} = this;
    return jwt.sign({
      id, username
    }, process.env.SECRET,
      {expiresIn: '1d'},
      );
  }
}
