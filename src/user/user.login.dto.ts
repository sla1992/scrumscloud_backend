import { IsString } from 'class-validator';

export class UserLoginDto{

  @IsString()
  email: string;

  @IsString()
  password: string;}

export class UserLoginRO {

  id : string;
  email: string;
  password: string;
  token?: string;
}