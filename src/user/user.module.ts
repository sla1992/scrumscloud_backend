import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { UserController } from './user.controller';
import { UserService } from './user.service';
import { UserEntity } from './user.entity';
import { OrganisationEntity } from '../organisation/organisation.entity';
import { FileEntity } from '../file/file.entity';
import { AuthService } from './auth/AuthService';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from '../user/auth/constants';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      UserEntity,
      FileEntity,
      OrganisationEntity
    ]),
    PassportModule,
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: { expiresIn: '60'}
    }),
  ],
  controllers: [
    UserController
  ],
  providers: [
    UserService,
    AuthService,
  ],
  exports: [AuthService],
})
export class UserModule {}
