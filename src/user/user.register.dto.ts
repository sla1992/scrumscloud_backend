import { IsString, IsHash } from 'class-validator';

export class UserRegisterDto{
  
  @IsString()
  id: string;
  
  @IsString()
  username: string;

  @IsString()
  email: string;
  
  @IsString()
  password: string;
  
}

export class UserRegisterRO {

  id : string;
  username: string;
  email: string;
  password: string;
  token?: string;
}
