import { Test, TestingModule } from '@nestjs/testing';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { ContextIdFactory } from '@nestjs/core';
import { TypeOrmModule, getRepositoryToken } from '@nestjs/typeorm';
import { UserEntity } from './user.entity';
import { FileEntity } from '../file/file.entity';
import { OrganisationEntity } from '../organisation/organisation.entity';
import { Repository } from 'typeorm';
import { testOrmConfig } from '../../test_ormconfig';
import { AuthService } from '../user/auth/AuthService';
import { JwtService } from '@nestjs/jwt';

describe('User Controller', () => {
  let controller: UserController;
  let service: UserService;
  let testUser: UserEntity;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [UserService],
      imports: [
        TypeOrmModule.forRoot(testOrmConfig),
        TypeOrmModule.forFeature([
          UserEntity,
          FileEntity,
          OrganisationEntity
        ]),
      ],
    }).compile();
    
    service = await moduleRef.get<UserService>(UserService);
  });

  describe('Get /users', () => {
    it('Should return an array of all users', async () => {
      expect(await service.showAll()).toStrictEqual([]);
    });
  });

  afterEach(async () =>{
    testOrmConfig.keepConnectionAlive = true;
  });
});
