import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { OrganisationEntity } from '../organisation/organisation.entity';
import { UserEntity } from './user.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { UserLoginDto, UserLoginRO } from './user.login.dto';
import { UserRegisterDto, UserRegisterRO } from './user.register.dto';
import { Hash } from 'crypto';
import * as bcrypt from 'bcrypt';
import { AuthService } from './auth/AuthService';


@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity) 
    private userRepository: Repository<UserEntity>,
    @InjectRepository(OrganisationEntity)
    private organisationRepository: Repository<OrganisationEntity>,
  ) {}
  
  async showAll(): Promise<UserRegisterRO[]> {
    const users = await this.userRepository.find({relations: ['fkorganisation']});
    return users.map(user => user.toResponseObject(false));
  }

  async login(data: UserLoginDto): Promise<UserLoginRO>{
    Logger.log(data);
    const {email, password} = data;
    const user = await this.userRepository.findOne({ where: { email } });
    // @ts-ignore
    if (!user || !(await user.comparePassword(password))){
      throw new HttpException(
        'Invalid username or password',
        HttpStatus.BAD_REQUEST,
      );
    }
    Logger.log(user.toResponseObject());
    return user.toResponseObject();
  }

  async register(data: UserRegisterDto): Promise<UserRegisterRO>{
    const  {username, password, email} = data;
    let userEmail = await this.userRepository.findOne({where: {email}});
    let userName = await this.userRepository.findOne({where: {username}})
    if (userEmail){
      throw new HttpException(
        'Email already exists',
        HttpStatus.BAD_REQUEST,
      );
    }
    if(userName){
      throw new HttpException(
        'Username already in use',
        HttpStatus.BAD_REQUEST,
      )
    }
    
    const user = this.userRepository.create({ username, password, email});
    await this.userRepository.save(user);
    return user.toResponseObject();
  }

  // return the user with the id that is given
  async read(id: string) {
    const user = await this.userRepository.findOne({ where: { id } });
    if(!user){
      throw new HttpException('User not Found', HttpStatus.NOT_FOUND);
    }
    return user;
  }

  // update the user in the DB
  async update(id: string, data: Partial<UserRegisterDto>) {
    const user = await this.userRepository.findOne({ where: { id } });
    if(!user){
      throw new HttpException('User not found', HttpStatus.NOT_FOUND);
    }
    user.email = data.email;

    // check if a password is send to update or not
    if(data.password){
      user.password = data.password;
    }
    user.password = await bcrypt.hash(user.password, 10);
    user.username = data.username;
    await this.userRepository.save(user);
    const updatedUser = await this.userRepository.findOne({ where: { id } });
    return updatedUser;
  }

  // delete user with some ID
  async delete(id: string) {
    const user = await this.userRepository.findOne({ where: { id } });
    if(!user){
      throw new HttpException('User not found', HttpStatus.NOT_FOUND);
    }
    await this.userRepository.delete({ id });
    return user;
  }
}
