import { TypeOrmModuleOptions } from "@nestjs/typeorm";
import { UserEntity } from "./src/user/user.entity";
import { FileEntity } from "./src/file/file.entity";
import { OrganisationEntity } from "./src/organisation/organisation.entity";

export const testOrmConfig: TypeOrmModuleOptions = {
  "type": "mysql",
  "host": "localhost",
  "port": 5432,
  "username": "root",
  "password": "root12345",
  "database": "scrumscloud_test",
  "synchronize": true,
  "logging": true,
  "entities": [UserEntity, FileEntity, OrganisationEntity]
}
